<?php

use App\Models\User;
use App\Models\Tree;
use App\Models\UserTree;
use Faker\Factory;

// Force these variables that are used in Overtrue WeChat
$_SERVER['HTTP_HOST'] = '';
$_SERVER['REQUEST_URI'] = '';

describe('Game mechanics', function () {
    beforeAll(function () {
        $faker = $faker = Factory::create();

        Artisan::call('migrate:refresh');
        Artisan::call('cache:clear');

        $this->users = [];
        for ($i = 0; $i < 50; $i++) {
            $openId = str_random(20);

            $this->users[] = [
                "open_id" => $openId,
                "params" => "?openid=".$openId."&nickname=".$faker->name."&headimgurl=".$faker->imageUrl($width = 60, $height = 60)."&forTest=true",
                "user" => null
            ];
        }

        /**
         * Get a user from an index
         */
        $this->user = function ($index) {
            return User::where('open_id', $this->users[$index]["open_id"])->first();
        };

        /**
         * Get a user's params from index
         */
        $this->userParams = function ($index) {
            return $this->users[$index]["params"];
        };

        /**
         * Get a user's open id from params
         */
        $this->userOpenId = function ($index) {
            return $this->users[$index]["open_id"];
        };

        /**
         * Get a user's tree from params
         */
        $this->userTree = function ($index) {
            $user = $this->user($index);
            return Tree::where('owner_id', $user->id)->first();
        };
    });

    it('creates a new tree and a new user when a user access the root URL for the first time', function () {
        $this->laravel->get('/'.$this->userParams(0))
            ->assertStatus(200)
            ->assertJson([
                "canLightStar" => "true",
            ]);

        expect(User::count())->toBe(1);
        expect(Tree::count())->toBe(1);
        expect(UserTree::count())->toBe(1);
        expect(Tree::first()->owner_id)->toBe($this->user(0)->id);
    });
    
    it('does not allow a tree owner to light a new star', function () {
        $firstUser = $this->user(0);
        $params = $this->userParams(0);

        $this->laravel->get('/'.$firstUser->public_id.$params)
            ->assertStatus(200)
            ->assertJson([
                "user" => [
                    "id" => $firstUser->id
                ],
                "canLightStar" => "false",
            ]);

        expect(User::count())->toBe(1);
    });
    
    it('displays the tree of the user whose public id is given as a route parameter', function () {
        $firstUser = $this->user(0);
        $params = $this->userParams(0);

        $this->laravel->get('/'.$firstUser->public_id.$params)
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $firstUser->id
                ],
            ]);

        expect(User::count())->toBe(1);
    });
    
    it('allows a user\'s friend to vote for his tree', function () {
        $owner = $this->user(0);
        $friendParams = $this->userParams(1);

        $this->laravel->get('/'.$owner->public_id.$friendParams)
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $owner->id
                ],
                "canLightStar" => "true",
            ]);

        // Recover tree and friend
        $ownerTree = $this->userTree(0);
        $friend = $this->user(1);

        expect(UserTree::where('tree_id', $ownerTree->id)->where('user_id', $friend->id)->count())->toBe(1);
    });
    
    it('does not allow a user\'s friend to vote twice', function () {
        $owner = $this->user(0);
        $friendParams = $this->userParams(1);

        $this->laravel->get('/'.$owner->public_id.$friendParams)
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $owner->id
                ],
                "canLightStar" => "false",
            ]);
    });

    it('does record the right amount of votes', function () {
        // Firstly get a tree for this user
        $this->laravel->get('/'.$this->userParams(1));

        // Get owner and tree
        $treeOwner = $this->user(1);
        $tree = $this->userTree(1);

        for ($i = 2; $i < 7; $i++) {
            $this->laravel->get('/'.$treeOwner->public_id.$this->userParams($i));
        }
        
        // Participants to the tree should be 1 (owner) + 5 (number of friends participating)
        expect(UserTree::where('tree_id', $tree->id)->count())->toBe(6);
    });
    
    it('does not allow a user to vote a tree if the tree is full', function () {
        // Create a tree
        $this->laravel->get('/'.$this->userParams(2));
        
        // Get owner and tree
        $treeOwner = $this->user(2);
        $tree = $this->userTree(2);

        // Full participation for this tree
        for ($i = 3; $i < 27; $i++) {
            $this->laravel->get('/'.$treeOwner->public_id.$this->userParams($i));
        }
        
        // Participants to the tree should be 1 (owner) + 24 (number of friends participating)
        expect(UserTree::where('tree_id', $tree->id)->count())->toBe(25);
        
        // New user tries to participate
        $this->laravel->get('/'.$treeOwner->public_id.$this->userParams(27))
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $this->user(27)->id
                ],
                "canLightStar" => "true",
            ]);
    });

    it('does show user\'s tree when user tries to access the campaign at root', function () {
        $this->laravel->get('/'.$this->userParams(0))
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $this->user(0)->id
                ],
            ]);
    });

    it('does show user\'s tree when user tries to access the campaign at root', function () {
        $this->laravel->get('/'.$this->userParams(0))
            ->assertStatus(200)
            ->assertJson([
                "tree" => [
                    "owner_id" => $this->user(0)->id
                ],
            ]);
    });
});