# Galeries Lafayette Christmas 2018

## Error codes

| Code  | Description                                            |
|  :--  |:-------------------------------------------------------|
|  0    | No error                                               | 
|  101  | User is not tree owner                                 | 