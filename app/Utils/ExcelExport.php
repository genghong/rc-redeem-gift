<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Carbon\Carbon;

class ExcelExport {

    // Define header cells' style
    public const HEADER_STYLE = [
        'font' => [
            'color' => ['argb' => 'FFFFFFFF'],
            'size' => '14'
        ],
        'fill' => [
            'fillType' => Fill::FILL_SOLID,
            'color' => ['argb' => 'FF5182BB'],
        ],
        'borders' => [
            'allBorders' => ['borderStyle' => Border::BORDER_THIN]
        ],
    ];

    // Define sub header cells' style
    public const SUB_HEADER_STYLE = [
        'fill' => [
            'fillType' => Fill::FILL_SOLID,
            'color' => ['argb' => 'FF8FBCEF'],
        ],
        'borders' => [
            'allBorders' => ['borderStyle' => Border::BORDER_THIN]
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
        ]
    ];

    // Set style style for other cells
    public const CELL_STYLE = [
        'borders' => [
            'allBorders' => ['borderStyle' => Border::BORDER_THIN]
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
        ]
    ];

    /**
     * Set header for your response (telling the browser this is an XLSX file)
     */
    public static function setHeaders ($fileName) {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
    }

    /**
     * Set a header cell.
     */
    public static function setHeaderCell($sheet, $column, $line, $content) {
        $sheet->setCellValue($column.$line, $content);
        $sheet->getStyle($column.$line)->applyFromArray(static::HEADER_STYLE);
        $sheet->getColumnDimension($column)->setAutoSize(true);
    }

    /**
     * Set a sub header cell (less important than a header)
     */
    public static function setSubHeaderCell($sheet, $column, $line, $content) {
        $sheet->setCellValue($column.$line, $content);
        $sheet->getStyle($column.$line)->applyFromArray(static::SUB_HEADER_STYLE);
    }

    /**
     * Set a simple cell
     */
    public static function setCell($sheet, $column, $line, $content) {
        $sheet->setCellValue($column.$line, $content);
        $sheet->getStyle($column.$line)->applyFromArray(static::CELL_STYLE);
    }

    /**
     * Output the XLSX file to the client's browser
     */
    public static function output($spreadsheet) {
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    /**
     * Fill the excel file with data. Data can be an array of columns or an array of a lines.
     */
    public static function fillExcel($sheet, $legends, $labels, $data, $totals = true, $dataAsColumns = true) {
        // The ASCII table offset is used to get the letter of the column we want to work on. If the table does not have
        // labels (first column headers) then we fill data and top header from column A
        $asciiOffset = !$labels || count($labels) == 0 ? 65 : 66;

        // Set first line headers, starting at column B
        foreach ($legends as $i => $header) {
            static::setHeaderCell($sheet, chr($asciiOffset+$i), 1, $header);
        }

        // Set first column headers
        $sheet->getColumnDimension('A')->setAutoSize(true);
        foreach ($labels as $i => $date) {
            static::setHeaderCell($sheet, 'A', $i+2, $date);
        }

        // Set table data
        $i = 0;     // Cannot use the values keys cause sometimes there are strings
        foreach ($data as $values) {
            $j = 0; // Cannot use the values keys cause sometimes there are strings
            foreach ($values as $value) {
                $column = $dataAsColumns ? chr($asciiOffset+$i) : chr($asciiOffset+$j);
                $line = $dataAsColumns ? $j+2 : $i+2;

                static::setCell($sheet, $column, $line, $value);

                $j++;
            }
            $i++;
        }   

        // Set totals
        if ($totals) {
            $line = 2 + count($data[0]);
            
            static::setHeaderCell($sheet, 'A', $line, 'Total');

            foreach ($legends as $i => $header) {
                $column = chr($asciiOffset+$i);
                static::setSubHeaderCell($sheet, chr($asciiOffset+$i), $line, '=sum('.$column.'2:'.$column.($line-1).')');
            }
        }
    }

    public static function dateLabels($from, $to = 'now') {
        $legends = [];
        $current = (new Carbon($from));
        $now = new Carbon($to);

        while ($current->lt($now)) {
            $legends[] = $current->toDateString();
            $current->addDay();
        }

        return $legends;
    }

}