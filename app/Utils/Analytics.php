<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Utils\ExcelExport;
use App\Models\User;
use App\Models\Tree;

class Analytics {

    public static function appendUsersTreesResults ($spreadsheet) {
        $sheet = new Worksheet($spreadsheet, 'Users Trees Results');
        $spreadsheet->addSheet($sheet);

        $result = \DB::table(\DB::raw('users u'))
            ->selectRaw("u.open_id, u.nickname, t.owner_id is not null as 'has_tree', t.name as 'tree_name', count(ut.user_id) as 'participant_friends'")
            ->leftJoin(\DB::raw('trees t'), \DB::raw('u.id'), 'owner_id')
            ->leftJoin(\DB::raw('user_trees ut'), function ($join) {
                $join->on(\DB::raw('t.id'), \DB::raw('ut.tree_id'))->on(\DB::raw('u.id'), '<>', \DB::raw('ut.user_id'));
            })
            ->groupBy(\DB::raw('u.open_id, u.nickname, has_tree, tree_name'))
            ->orderByRaw('has_tree desc, participant_friends desc')
            ->get()->toArray();

        // Gather you data here
        $legends = ['Open ID', 'Nickname', 'Has Tree', 'Tree Name', 'Friends Participating to Tree'];
        $data = $result;

        $labels = [];

        // Fill the excel based on the data we previously got
        ExcelExport::fillExcel($sheet, $legends, $labels, $data, false, false);
    }

    public static function appendUsersParticipationResults ($spreadsheet) {
        $sheet = new Worksheet($spreadsheet, 'Users Participation Results');
        $spreadsheet->addSheet($sheet);

        $result = \DB::table(\DB::raw('users u'))
            ->selectRaw("u.open_id, u.nickname, count(ut.user_id) as 'participations'")
            ->leftJoin(\DB::raw('trees t'), \DB::raw('u.id'), 'owner_id')
            ->leftJoin(\DB::raw('user_trees ut'), function ($join) {
                $join->on(\DB::raw('u.id'), \DB::raw('ut.user_id'))->on(\DB::raw('ut.tree_id'), '<>', \DB::raw('t.id'));
            })
            ->groupBy(\DB::raw('u.open_id, u.nickname'))
            ->orderByRaw('participations desc')
            ->get()->toArray();

        // Gather you data here
        $legends = ['Open ID', 'Nickname', 'Has Participated to Trees'];
        $data = $result;

        $labels = [];

        // Fill the excel based on the data we previously got
        ExcelExport::fillExcel($sheet, $legends, $labels, $data, false, false);
    }

}