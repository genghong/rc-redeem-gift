<?php

namespace App\Utils;

/**
 * This is for using when you need custom errors, for example if you don't use form data validation.
 * Prefer to use form data validation when it's possible.
 */
class Error {

    const ERRORS = [
        100 => 'Error message',
    ];

    public static function error($code) {
        if (!array_key_exists($code, static::ERRORS)) {
            throw new \Exception('The error code ' . $code . ' does not exist');
        }

        return [
            'error' => $code,
            'message' => static::ERRORS[$code],
        ];
    }

}