<?php

namespace App\Http\Middleware;

use Closure;
use EasyWeChat\Factory;

class WeChatJSSDK
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Initialize WeChat
        $app = app('wechat.official_account');

        // Get JSSDK
        $jssdk = $app->jssdk->buildConfig(
            ['onMenuShareTimeline', 'onMenuShareAppMessage'],
            false,      // Debug or not
            false,
            false
        );

        // Ensure the request gets the JSSDK value
        $request->attributes->add(['jssdk' => $jssdk]);

        return $next($request);
    }
}
