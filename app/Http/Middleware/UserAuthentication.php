<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Faker\Factory;

class UserAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $openId = session('openId') ?? $request->input('openid');
        $openId = session('wechat.oauth_user.default')->id;

        // If we are in testing environment, using unit tests, priorize request inputs
        if (config('app.env') !== 'production' && $request->input('forTest')) {
            $openId = $request->input('openid');
        }

        // If we are not in production and we want to mockup a user
        if (config('app.env') !== 'production' && config('app.wechat_mockup') && (!$openId)) {
            $faker = Factory::create();

            // Get or set fake data
            $openId = \Session::get('openId') ?? str_random(20);
        }

        // Save user information in session along with request params if there are any
        session(['openId' => $openId]);

        // Save session cause we will probably redirect the session
        \Session::save();
        
        // If we have all required information
        if ($openId) {
            // Try to get a user having this OpenID
            $user = User::fromOpenId($openId);

            // Register user's information
            if (!$user) {
                $user = new User();
                $user->open_id = $openId;
                $user->save();
            }
            $request->attributes->add([
                'user' => $user
                ]);
            // return $next($request);
            
        } 
        // else {
        //     \Log::info('Redirect to ' . config('app.wechat_auth_proxy'));
        //     return redirect(config('app.wechat_auth_proxy'));
        // }
        return $next($request);
    }
}
