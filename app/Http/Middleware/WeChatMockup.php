<?php

namespace App\Http\Middleware;

use Closure;
use Overtrue\Socialite\User as SocialiteUser;
use Faker\Factory;

class WeChatMockup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         /**
         * Only if local dev
         */
        if (config('app.env') !== 'production' && config('app.wechat_mockup'))
        {
            $faker = Factory::create();

            $user = new SocialiteUser([
                'id' => str_random(20),
                'name' => $faker->name,
                'nickname' => $faker->userName,
                'avatar' => '',
                'email' => null,
                'original' => [],
                'provider' => 'WeChat',
            ]);

            session(['wechat.oauth_user.default' => $user]);
        }

        return $next($request);
    }
}
