<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Utils\ExcelExport;
use App\Utils\Analytics;
use JavaScript;
use Carbon\Carbon;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function home (Request $request) {

        $user = $request->get('user');
        // If game is not finish
        $user = User::where('open_id', $user->open_id)->first();
        if(Carbon::now() < config('app.end_date')){
            $forTest = $request->get('forTest');
            // Construct the params array
            $params = [
                'user' => $user,
                'redeemed_gift' => $user->redeemed_gift,
            ];
            
            // If the forTest variable is given, we send only a json object (for unit and jmeter testing)
            if ($forTest) {
                return $params;
            } else {
                Javascript::put([
                    'params' => $params,
                    'jssdk' => $request->get('jssdk'),
                ]);
                return view('home');
            }
    }
}

    /**
     * Export an excel analytics file
     */
    public function export () {
        // Init the spreadsheet
        $spreadsheet = new Spreadsheet();
        $spreadsheet->removeSheetByIndex(0);

                // Add users trees results
        Analytics::appendUsersTreesResults($spreadsheet);

        // Add users participation results
        Analytics::appendUsersParticipationResults($spreadsheet);

        // Redirect output to a client’s web browser (Xls)
        ExcelExport::setHeaders(Carbon::now()->toDateString() . '_analytics.xlsx');

        // Final output
        ExcelExport::output($spreadsheet);
    }

}
