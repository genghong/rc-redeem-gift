<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\User;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function redeemGift (Request $request) {
        $user_id  = $request->user_id;
        $user = User::where('id', $user_id)->first();
        $user->redeemed_gift = 1;
        $user->save();

        return ['error' => 0];
    }
}
