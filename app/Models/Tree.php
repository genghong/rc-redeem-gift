<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use App\Models\UserTree;
// use App\Models\User;

class Tree extends Model
{
    //
    public static function fromPublicId ($publicId) {
        $user = User::fromPublicId($publicId);

        // If no user exists with the given public id
        if (!$user) {
            return null;
        }

        return Cache::rememberForever(static::cacheKey($publicId), function () use ($user, $publicId) {
            return Tree::where('owner_id', $user->id)->first();
        });
    }

    public function forgetCache () {
        $user = User::where('id', $this->owner_id)->first();
        \Cache::forget(static::cacheKey($user->public_id));
    }

    public static function cacheKey($publicId) {
        return 'tree_' . $publicId;
    }

    public function incrementStarsCount() {
        $key = 'stars_count_' . $this->id;
        // We make sure the stars count cached value for this tree exists
        \Cache::rememberForever($key, function () {
            return UserTree::where('tree_id', $this->id)->count();
        });

        // We increment the stars count
        return \Cache::increment($key, 1);
    }

    public static function newTree ($user) {
        $tree = new Tree();
        $tree->owner_id = $user->id;
        $tree->save();
        UserTree::insert(['user_id'=>$user->id,'tree_id'=>$tree->id]);
        return $tree;
    }

    public function starsCount () {
        return UserTree::where('tree_id', $this->id)->count();
    }
}
