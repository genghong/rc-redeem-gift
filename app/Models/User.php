<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Tree;

class User extends Model
{
    protected static function boot() {
        parent::boot();

        // When a new agent is created, add to the last rank.
        static::created(function (User $user) {
            // $user->public_id = md5($user->open_id);
            $user->save();
        });
    }

    public static function fromOpenId ($openId) {
        return Cache::rememberForever(User::cacheKey($openId), function () use ($openId) {
            return User::where('open_id', $openId)->first();
        });
    }

    public static function fromPublicId ($openId) {
        return Cache::rememberForever(User::cacheKey($publicId), function () use ($publicId) {
            return $user =  User::where('public_id', $publicId)->first();
        });
    }

    public function forget() {
        // Cache::forget(User::cacheKey($this->public_id));
        Cache::forget(User::cacheKey($this->open_id));
    }

    public static function cacheKey($openId) {
        return 'user_'.$openId;
    }

    public function trees() {
        return $this->hasMany('App\Models\Tree')->orderBy('id');
    }

}
