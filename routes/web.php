<?php

/**
 * If you need to get extra information such as user's nickname or city, append ':snsapi_userinfo' to 'wechat.oauth'
 */

Route::get('/', 'Controller@home')->middleware(['wechat.oauth','wechat.jssdk', 'user.auth']);

Route::get('/export', 'Controller@export')->middleware('basic.auth.custom');

Route::any('/draw', 'Controller@drawWinner')->middleware('basic.auth.custom');

Route::get('/{fromUser?}', 'Controller@home')->middleware(['wechat.jssdk', 'user.auth']);


// Route::get('/export', 'Controller@export')->middleware('basic.auth.custom');

// Route::any('/draw', 'Controller@drawWinner')->middleware('basic.auth.custom');

// Route::get('/{fromUser?}', 'Controller@home')->middleware('user.auth');