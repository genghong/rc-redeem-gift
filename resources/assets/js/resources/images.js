module.exports = [
    require('../../images/background/main-background.jpg'),
    require('../../images/gif/original.gif'),
    require('../../images/gif/totebag.gif'),
    require('../../images/gif/down.gif'),
    require('../../images/icon/logo.png'),
    require('../../images/icon/share-blue.png'),	
    require('../../images/icon/share-box.png'),	
    require('../../images/icon/share-green.png'),	
    require('../../images/icon/input-box-image.png'),	
    require('../../images/icon/title.png'),	
    require('../../images/rotate-your-phone.svg'),
    require('../../images/share-picture.jpg'),
    require('../../images/animation.png')
]


