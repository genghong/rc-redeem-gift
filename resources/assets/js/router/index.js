import Vue from 'vue'
import Router from 'vue-router'
import Data from '../utils/data'
import Home from '../pages/Home.vue'
import RedeemedGiftPage from '../pages/RedeemedGiftPage.vue'
import RedeemGiftPage from '../pages/RedeemGiftPage.vue'

Vue.use(Router)

var router = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            props: true
        },
        {
            path: '/redeemGiftPage',
            name: 'redeemGiftPage',
            component: RedeemGiftPage,
            props: true
        },
        {
            path: '/redeemedGiftPage',
            name: 'redeemedGiftPage',
            component: RedeemedGiftPage,
            props: true
        },
    ]
})

router.beforeEach((to, from, next) => {
    if (!Data.isRouterGuardEnabled) {
        return next()
    }

    // Add your custom guarding rules here

    next()
})

export default router