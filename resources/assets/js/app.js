import App from './pages/App.vue'
import router from './router'
import AppUtils from './utils/app-utils'
import API from './utils/api'
import Data from './utils/data'
import WeChat from './utils/wechat'

import './locale/ml'
import './utils/polyfill'

require('./bootstrap')              // Use Bootstrap, can be always useful

window.Vue = require('vue');

// Create an event bus for global events
window.eventBus = new Vue()

// Use custom plugins
Vue.use(AppUtils)
Vue.use(API)

// Setup the Vue app
const vueApp = new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})

// Background music controls
window.eventBus.$on('play-music', () => {
    document.getElementById('background-music').play();
})
window.eventBus.$on('pause-music', () => {
    document.getElementById('background-music').pause();
})

// Initialize WeChat
WeChat.init(app.jssdk)
    .then(() => {
        if (Data.soundEnabled) {
            window.eventBus.$emit('play-music')
        }
    })
    .catch(error => {
        if (Data.soundEnabled) {
            window.eventBus.$emit('play-music')
        }
    })