import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'cn',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('cn').create(require('./cn')),
  ]
})