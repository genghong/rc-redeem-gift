module.exports = {
    'homePage':{
        'content_top': '<li>欢迎来到<li>Roberto Cavalli</li>',
        'content_mid': '<li>Roberto Cavalli香港海港城限时精品店</li><li>为您准备了一份专属惊喜</li><li>轻松三步即可拥有Roberto Cavalli</li><li>独家定制帆布袋</li>',
        'step_1': '<li>关注Roberto Cavalli</li><li>官方微信公众号，加入RC家族</li>',
        'step_2':'<li>亲临海港城2221店铺拍摄现场照片</li><li>带上话题 <font style="color:#009790">#RCSpecies#</font> </li><li>并且<font style="color:#006aa8"> #Robertocavalli</font></li><li>在任意个人社交平台</li><li>（包括Instagram、微信朋友圈、微博）</li><li>发布动态，成为朋友圈“新”icon</li>',
        'step_3': '<li>完成以上步骤后</li><li>请店内工作人员<li></li>在本页面下方输入通关密语</li><li>即可获得独家定制帆布袋</li>',
        'bottom_context':'</li><li>（ *本次活动礼品数量有限，先到先得。</li><li>本活动每个微信账号仅限参与一次，</li><li>最终解释权归Roberto Cavalli所有。）',
        'sharePagePop': '<li>感谢您!</li><li>帮助{ownerName}点亮了第{starsCount}颗星星。</li><li>如果你也想要赢得巴黎老佛爷的圣诞惊喜，就点击下面的按钮来一起点亮专属于你的圣诞树吧!</li>',
        'redeemGiftPage':'<li>恭喜您，完成任务！</li><li>您已成功获得Roberto Cavalli</li><li> “新物种时代”独家定制帆布袋。</li><li>赶快现场创作您心中的“新物种”吧! </li>',
        'wrongPwd': '<li>对不起，您输入的</li><li>通关密语错误！请重新输入。</li>'
    },
    
    'redeemGiftPage':{
        'congratFinish': '<li>恭喜您，完成任务！</li><li>您已成功获得Roberto Cavalli</li><li> “新物种时代”独家定制帆布袋。</li><li>赶快现场创作您心中的“新物种”吧! </li>',
        'thankOwnerMsg': '<li>太棒了，你已成功点亮了第1颗圣诞星！</li>',
        'thankFriendMsg': '<li>感谢您，</li><li>帮助{ownerName}点亮了第{starsCount}颗圣诞星！</li>',
        'followTip': '<li>关注巴黎老佛爷公众号，第一时间获取获奖名单！👇</li>',
        'ownerTip': '<li>点击“分享”，转发给小伙伴或转发至朋友圈，点亮所有圣诞星，赢得巴黎老佛爷的圣诞惊喜！👇</li>',
        'friendTip': '<li>点击“分享”，去告诉{ownerName}圣诞星点亮进度！</li><li>或者，请别的朋友帮Olivia点亮剩余的圣诞星。👇</li>',
        'finishTip': '<li>点击“分享”，</li><li>去告诉{ownerName}，</li><li>圣诞星已经全部点亮!👇</li>',
        'friendAgainMsg': '<li>{ownerName}的极光圣诞树已经有{starsCount}颗圣诞星被点亮了！<li>',
        'ownerAgainMsg': '<li>恭喜你！你的极光圣诞树已经有{starsCount}颗圣诞星被点亮了！</li>',
        'followAndFocus':'<li>长按二维码，</li><li>关注巴黎老佛爷公众号，</li><li>第一时间获取获奖名单！</li>'
    },
    'redeemedGiftPage': {
        'congratFinish': '<li>您已成功获得Roberto Cavalli</li><li>“新物种时代”独家定制帆布袋。</li>',
        'inviteFriend': '<li>邀请好友一同参与，成为时尚新icon！</li>'

    },
    'share':{
        'shareWords': '<li>点击右上角</li><li>分享给你的好友</li>',
    },
    'treePage':{
        'thankTop': '<li>你好，我是Sadanse!</li>',
        'thankFinishMsg': '<li>感谢你的参与，</li><li>{ownerName}的极光圣诞树已经完成了！</li><li>你也想赢得巴黎老佛爷的圣诞惊喜吗？</li><li>点击下面的按钮，</li><li>创建属于你的极光圣诞树！</li>',
        'namedAndLight': '<li>恭喜你成功命名了自己的极光圣诞树！</li><li>点击右上角按钮，点亮第一颗圣诞星。</li>'
    },
    'general': {
        'network-error-message': `对不起，发生网络链接错误，请重试`,
        'ok': 'OK',
    },
    'terms': {
        'title': `服务条款`,
        'content': `<p>游戏规则&nbsp;</p>
        <ol>
        <li>根据页面提示，选择你喜欢的锁，填写你与好友的姓名，并选择背景中桥上的任意位置，将锁悬挂于该位置，即完成操作。</li>
        </ol>
        <ul>
        <li>一等奖：1名，将获得免费巴黎双人游的机会， 具体内容包括：
        <ul style="list-style-type: square;">
        <li>两张国内城市直飞巴黎的往返机票</li>
        <li>巴黎市中心五星级酒店 W-Paris Op&eacute;ra三晚住宿</li>
        <li>Lido巴黎丽都歌舞剧院晚餐以及歌舞秀2位</li>
        </ul>
        </li>
        </ul>
        <p>活动条款</p>
        <p>参与时间及方式：</p>
        <ol>
        <li>本次活动时间为2018年4月16日21:00:00（北京时间）至2018年4月26日23:59:59（北京时间）</li>
        <li>分享游戏结果给朋友或至朋友圈。</li>
        <li>评选：在推广期间，将产生6名获奖者，其中一等奖1名，二等奖5名。巴黎老佛爷奥斯曼旗舰店将人工评定和最终评选此次活动的大奖得主。</li>
        <li>奖品：一等奖：巴黎老佛爷提供的免费巴黎双人游套餐，包括两张国内城市直飞巴黎的经济舱往返机票，W-Paris Op&eacute;ra酒店三晚住宿，巴黎丽都歌舞剧场晚餐以及歌舞秀两位，塞纳河游船餐厅午餐两位。二等奖：Diptyque 香薰蜡烛一只。除巴黎老佛爷奥斯曼旗舰店外，任何人不得转让、分配或替换奖品或以奖品兑换现金。巴黎老佛爷奥斯曼旗舰店保留以价值相当或更高之物替换奖品的权利，由巴黎老佛爷奥斯曼旗舰店全权决定。所有奖品不提供担保和保修服务，奖品产生的任何税费或其他费用均由获奖者承担。</li>
        <li>6名奖获得者会在2018年4月27日在巴黎老佛爷奥斯曼旗舰店微信平台&ldquo;微信名称：巴黎老佛爷百货公司&rdquo;上公布。获奖用户必须在7日内回复确认并回复相关个人信息，否则视为放弃奖品，主办方保留该奖品授予其他参与者的权利。由于信息有误而造成奖品无法准确送达的，巴黎老佛爷奥斯曼旗舰店概不负责。</li>
        <li>本次活动一等奖奖品中的&ldquo;两张国内城市直飞巴黎往返机票&rdquo;，航空公司为任意含有直飞巴黎航班的航空公司，舱位为经济舱，起飞城市适用于国内含有直飞巴黎航班的城市，有效期为2018年5月2日至2018年12月30日。具体信息根据预定要求与航空公司舱位情况决定。</li>
        <li>本次活动奖项设置中&ldquo;W Paris - Op&eacute;ra酒店三晚住宿&rdquo;仅适用于W Paris - Op&eacute;ra，适用于2018年5月2日至12月30日的单人间或双人间。具体预定详情根据酒店客房空余情况决定。</li>
        <li>本次活动一等奖奖品中的&ldquo;丽都歌舞剧院晚餐以及歌舞秀&rdquo;以及&ldquo;塞纳河游船餐厅午餐&rdquo;的适用时段为周日至周四，具体时间以营业时间为准。</li>
        <li>本次活动一等奖获奖者认领奖品之后，需要在旅行行程前一个月与巴黎老佛爷百货的工作人员联系安排机票、酒店以及丽都歌舞剧场和塞纳河游船餐厅的行程预定与安排。</li>
        <li>本次活动二等奖中的&ldquo;法国知名香氛品牌Diptyque 香薰蜡烛&rdquo;具体蜡烛的产品型号将由巴黎老佛爷百货随机选取，并以快递方式发送给获奖者。</li>
        <li>本次活动三等奖奖品中的老佛爷百货的礼品条形码使用时间为2018年5月2日到2018年12月31日，自由行顾客凭借此条形码在巴黎老佛爷奥斯曼旗舰店购物满50欧元即可获赠老佛爷百货logo购物袋。礼物数量有限，送完即止。</li>
        <li>所有参与者与本活动，即视为同意并遵守以上各项规定。</li>
        <li>本活动的最终解释权归巴黎老佛爷奥斯曼旗舰店（微信名称：巴黎老佛爷百货公司）所有。</li>
        </ol>`
    },
}