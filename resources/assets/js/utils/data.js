var soundEnabled = false

export default {

    set soundEnabled (value) {
        soundEnabled = value
    },

    get soundEnabled () {
        return soundEnabled
    },

}