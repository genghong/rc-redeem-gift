
// Get the base URL
var baseUrl = window.location.origin

var title = '和我一起成为Roberto Cavalli“一日设计师”'
var description = '点击领取你的专属定制帆布袋'
var imageUrl = `${baseUrl}/images/share-picture.jpg`

// Set default share to the game
// var setDefaultShare = function (appenToUrl = '') {
var setDefaultShare = function () {
    wx.onMenuShareTimeline({
        title: title,
        link: `${baseUrl}`,
        imgUrl: imageUrl
    })

    wx.onMenuShareAppMessage({
        title: title,
        desc: description,
        link: `${baseUrl}`,
        imgUrl: imageUrl
    })

}

// var appendToUrl = function (value) {
//     setDefaultShare(value)
// }

/**
 * Init the WeChat JS SDK
 * @param {Object} jssdk The JS SDK configuration oject
 */
var init = (jssdk) => {
    return new Promise((resolve, reject) => {
        // Initialize WeChat
        wx.config(jssdk)

        // Default sharing values
        wx.ready(function () {
            setDefaultShare()
            resolve()
        })

        // Error handling
        wx.error(function(res){
            reject()
        })
    })
}

export default {
    init: init,
    // appendToUrl: appendToUrl,
}
