import Axios from 'axios'
import MobileDetect from 'mobile-detect'
import Localforage from 'localforage'

var md = new MobileDetect(window.navigator.userAgent)
var initialHeight = document.documentElement.clientHeight

const AppUtils = {
    install (Vue, options) {
        Vue.mixin({
            data () {
                return {
                    keyboardOpened: false,
                    isPortrait: false,
                    
                    // For development => Change the last boolean for testing functionalities in dev env
                    debug: process.env.NODE_ENV !== 'production' && false,
                    isRouterGuardEnabled: process.env.NODE_ENV === 'production' || false,
                    shouldShowLiveLogs: process.env.NODE_ENV !== 'production' && false,
                    shouldShowDevMessages: process.env.NODE_ENV !== 'production' && false,
                }
            },
            mounted () {
                // Manage visibility of elements when keyboard is out
                // window.addEventListener('resize', () => {
                    // if (this.isPortrait && document.documentElement.clientHeight < document.documentElement.clientWidth) {
                    //     initialHeight = document.documentElement.clientHeight
                    // }
    
                    // this.keyboardOpened = document.documentElement.clientHeight !== initialHeight
                // })
    
                // Check is wrong orientation
                if (document.documentElement.clientHeight > document.documentElement.clientWidth) {
                    this.isPortrait = true
                }
            },
            methods: {
                /**
                 * Tell the main app appge to display a specified message in a popup with an ok button
                 */
                showPopup (message) {
                    window.eventBus.$emit('showPopup', message)
                },

                /**
                 * Tell the main app appge to display a network error message
                 */
                showNetworkErrorPopup () {
                    window.eventBus.$emit('networkError')
                },

                /**
                 * Tell the main app appge to display a loading indicator
                 */
                showLoading () {
                    window.eventBus.$emit('showLoading')
                },

                /**
                 * Tell the main app appge to hide a loading indicator
                 */
                hideLoading () {
                    window.eventBus.$emit('hideLoading')
                },

                /**
                 * Download a resource and store it locally using local forage
                 * @param {String} key The key used in local forage for storing the resource
                 * @param {String} url URL to the resource to be downloaded
                 */
                downloadAndStore (key, url) {
                    return new Promise((resolve, reject) => {
                        Axios.get(url, {responseType: 'blob'})
                            .then(result => {
                                Localforage.setItem(key, result.data)
                                    .then(() => {
                                        resolve(result.data)
                                    })
                            })
                            .catch(error => {
                                reject(error)
                            })
                    })
                },

                /**
                 * Force loading an image even if it's not used yet.
                 * REMARK: didn't want to delete the other loadImage function cause it's simpler, but you can do it if
                 * later it's totally useless.
                 * @param {String} src URL to the image to be loaded
                 */
                loadImageWithProgress (src, onProgress) {
                    return new Promise(resolve => {
                        var image = new Image()
    
                        // Load callback
                        image.onload = () => {
                            resolve(image)
                        }

                        // On loading progress
                        image.onProgress = onProgress
    
                        // Handle loading error
                        image.onerror = (error) => {
                            image.attempts = image.attempts == null ? 1 : image.attempts + 1
    
                            // If still has attempt, try to load again
                            if (image.attempts == 5) {
                                resolve(image)
                            } else {
                                image.src = src
                            }
                        }
    
                        image.load(src)
                    })
                },
                
                /**
                 * Load a sound resource from local forage
                 * @param {String} soundKey The key to the sound resource in local forage
                 */
                loadSound (soundKey) {
                    return new Promise((resolve, reject) => {
                        // Initialize the mat appearance sound
                        var reader = new FileReader()
                        Localforage.getItem(soundKey, function (error, value) {
                            if (error) {
                                reject(error)
                            } else {
                                reader.onload = function (e) {
                                    var htmlAudio = new Audio()
                                    var source = document.createElement("source")
                                    source.type = "audio/mpeg"
                                    source.src = e.target.result
                                    htmlAudio.appendChild(source)
                                    resolve(htmlAudio, e.target.result)
                                }
        
                                // If a problem occurs, try once again
                                reader.onabort = reader.onerror = function (e) {
                                    console.error('Error on loading sound. Retrying...')
                                    if (!this.alreadyTried) {
                                        this.alreadyTried = true
                                        this.readAsDataURL(value)
                                    } else {
                                        reject('Could not load the sound after 2 attempts')
                                    }
                                }
        
                                reader.readAsDataURL(value)
                            }
                        })
                    })
                },

                /**
                 * Get a data URL from an image file.
                 * @param {File} data File representation of the image
                 */
                toDataURL (data) {
                    var urlCreator = window.URL || window.webkitURL
                    return urlCreator.createObjectURL(data)
                },

                /**
                 * Tell if the current device is iOS
                 */
                isIOS () {
                    return md.is('iPhone')
                },

                /**
                 * Tell if the current device is iOS 11
                 */
                isIOS11 () {
                    if (md.is('iPhone') &&  md.version('iOS') >= 11) {
                        return true
                    } 

                    return false
                },

                hasClass(el, className) {
                    if (el.classList)
                        return el.classList.contains(className)
                    else
                        return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
                },
                    
                addClass(el, className) {
                    if (el.classList)
                        el.classList.add(className)
                    else if (!hasClass(el, className)) el.className += " " + className
                },
                    
                removeClass(el, className) {
                    if (el.classList)
                        el.classList.remove(className)
                    else if (hasClass(el, className)) {
                        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
                        el.className=el.className.replace(reg, ' ')
                    }
                },

                /**
                 * Tells if el's center is contained inside container
                 * @param {*} container 
                 * @param {*} el 
                 */
                containsCenter(container, el) {
                    let containerBox = container.getBoundingClientRect()
                    let elBox = el.getBoundingClientRect()

                    let elCenter = {
                        x: elBox.x + elBox.width / 2,
                        y: elBox.y + elBox.height / 2,
                    }

                    return  elCenter.x >= containerBox.x && elCenter.x <= containerBox.x + containerBox.width &&
                            elCenter.y >= containerBox.y && elCenter.y <= containerBox.y + containerBox.height
                },
                
                /**
                 * Return the distance between 2 elements' respective centers
                 * @param {*} el1 
                 * @param {*} el2 
                 */
                centerDistance(el1, el2) {
                    let el1Box = el1.getBoundingClientRect()
                    let el2Box = el2.getBoundingClientRect()
    
                    let el1Center = {
                        x: el1Box.x + el1Box.width / 2,
                        y: el1Box.y + el1Box.height / 2,
                    }
    
                    let el2Center = {
                        x: el2Box.x + el2Box.width / 2,
                        y: el2Box.y + el2Box.height / 2,
                    }

                    let xDiff = el1Center.x - el2Center.x
                    let yDiff = el1Center.y - el2Center.y

                    return Math.sqrt(xDiff * xDiff + yDiff * yDiff)
                },

                /**
                 * Loads an image src and return an image element
                 */
                loadImage (src) {
                    return new Promise ((resolve, reject) => {
                        let image = new Image()
                        image.onload = () => resolve(image)
                        image.onerror = reject
                        image.src = src
                    })
                },

                /**
                 * Return an image's data URL from a file input
                 * @param {*} fileInput 
                 */
                fileInputToDataURL (fileInput) {
                    return new Promise ((resolve, reject) => {
                        var reader = new FileReader();

                        reader.onload = e => {
                            resolve(e.target.result)
                        }

                        reader.onerror = reject

                        reader.readAsDataURL(fileInput);
                    })
                }, 
            }
        })
    }
}

export default AppUtils;