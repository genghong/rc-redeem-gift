import Data from './data'

/**
 * Common params for all Axios requests
 */
var axios = require('axios').create({
    timeout: 10000,
})

/**
 * Helper for converting base64 to blob
 */
var BASE64_MARKER = ';base64,'
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || ''
    sliceSize = sliceSize || 512

    var base64Index = b64Data.indexOf(BASE64_MARKER) + BASE64_MARKER.length
    b64Data = b64Data.substring(base64Index)
  
    var byteCharacters = atob(b64Data)
    var byteArrays = []
  
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize)

        var byteNumbers = new Array(slice.length)
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i)
        }

        var byteArray = new Uint8Array(byteNumbers)

        byteArrays.push(byteArray)
    }
  
    var blob = new Blob(byteArrays, {type: contentType})
    return blob
}

/**
 * Retry if a request fails
 */
axios.interceptors.response.use(undefined, function axiosRetryInterceptor(err) {
    var config = err.config;
    // If config does not exist or the retry option is not set, reject
    if(!config || !config.retry) return Promise.reject(err);
    
    // Set the variable for keeping track of the retry count
    config.__retryCount = config.__retryCount || 0;
    
    // Check if we've maxed out the total number of retries
    if(config.__retryCount >= config.retry) {
        // Reject with the error
        return Promise.reject(err);
    }
    
    // Increase the retry count
    config.__retryCount += 1;
    
    // Create new promise to handle exponential backoff
    var backoff = new Promise(function(resolve) {
        setTimeout(function() {
            resolve();
        }, config.retryDelay || 1);
    });
    
    // Return the promise in which recalls axios to retry the request
    return backoff.then(function() {
        return axios(config);
    });
});

const API = {
    install (Vue, options) {
        Vue.mixin({
            methods: {
                redeem (user_id) {
                    return new Promise((resolve, reject) => {
                        var data = {
                            user_id:user_id,
                        }
                        axios.put(`/api/redeem`,data)
                        .then(function (res) {
                            resolve(res.data)
                        })
                        .catch(function (error) {
                            reject(error)
                        })
                    })
                },
            }
        })
    }
}

export default API;