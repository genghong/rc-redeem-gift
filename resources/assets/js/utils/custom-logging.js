/**
 * Replace the default console logging system for calling window.log
 * window.log is defined in the App.vue file
 * TODO: treat differently custom warning and error
 */

var _log = console.log;
var _error = console.error;
var _warning = console.warning;

console.error = function(errMessage){
    window.log(errMessage)
    _error.apply(console,arguments);
};

console.log = function(logMessage){
    window.log(logMessage)
    _log.apply(console,arguments);
};

console.warning = function(warnMessage){
    window.log(warnMessage)
    _warning.apply(console,arguments);
};