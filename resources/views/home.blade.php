<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        {{-- <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"> --}}
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi, viewport-fit=cover" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <!--
            The background music element, used for loading the music as soon as possible.
        -->
        <!-- <audio id="background-music" src="{{ asset('sounds/music.mp3') }}" preload loop autoplay></audio> -->

        <!--
            This the main page content. Main page is static an should contain only lightweight elements for quick loading.
            This page should generally contain a terms and conditions button and a "Start" button.
        -->
        <div id="home-page-content">
            <div id="home-page-loading-message">
                <div id="loading">
                    <img id="loading-animation" class="play-half-rotate" src="{{ asset('images/animation.png') }}" />
                    <div id="progress">0%</div>
                </div>
            </div>
        </div>

        <!--
            Hack for not seeing any static content when navigating inside the Vue application
        -->
        <div id="white-layer"></div>

        <!--
            The rotate phone page tells the user to rotate the phone if it's not the right orientation. You can change
            the required orientation in the app.scss file.
        -->
        <div id="rotate-phone" class="rotate-phone-media">
            <img id="logo" src="{{ asset('images/icon/logo.png') }}"/>
            <div class="title">
                <img src="../../images/icon/title.png"/>
            </div>
            <div id="rotate-message">请保持手机垂直模式放置</div>
            <div id="rotate-animation">
                <img id="rotate-animation" class="play-rotate" src="{{ asset('images/rotate-your-phone.svg') }}" />
            </div>
            <!-- <div id="android-rotate-message">Special rotate message for android phones (wechat config needed)</div> -->
        </div>

        <!--
            The Vue app, the heart of your application
        -->
        <div id="app"></div>
    </body>

    <!-- Hide the special Android rotate message if on iOS -->
    <script>
        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)
        if (iOS) {
            document.getElementById('android-rotate-message').remove()
        }
    </script>

    <!-- Import WeChat JS SDK library -->
    <script src="//res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>

    {{-- Append javascript variables injected from the controller --}}
    @include('javascript')

    <!-- Import main app -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Import main app -->
</html>
