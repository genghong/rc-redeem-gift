<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            @if ($winner)
                <h1>Here is the winner !</h1>
                <h3>{{ $winner->nickname }}</h3>
                <h4>Tree's name: {{ $winner->tree_name }}</h4>
                <img width="300px" style="object-fit: contain; object-position: center;" src="{{ $winner->avatar }}"/>
            @else
                <h1>Input the password to draw the winner. Once it's been draw, you cannot change the winner.</h1>
                <form method="post">
                    @csrf
                    <input type="text" placeholder="Input password" name="password"/>
                    <button class="btn">Draw Winner!</button>
                </form>
            @endif
        </div>
    </body>

    <style>
        form {
            margin-top: 4vh;
        }
    </style>
</html>
